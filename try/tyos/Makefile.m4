
dnl  Makefile.m4 - Makefile template.
dnl    
dnl  Copyright (c) 2021 - Monaco F. J. <monaco@usp.br>
dnl
dnl  This file is part of SYSeg. 
dnl
dnl  SYSeg is free software: you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation, either version 3 of the License, or
dnl  (at your option) any later version.
dnl
dnl  SYSeg is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with .  If not, see <http://www.gnu.org/licenses/>.
dnl
dnl  ------------------------------------------------------------------
dnl
dnl  Note: this is a source file used to produce either a documentation item,
dnl        script or another source file by m4 macro processor. If you've come
dnl	   across a file named, say foo.m4, while looking for one of the
dnl	   aforementioned items, changes are you've been looking for file foo,
dnl	   instead. If you can't find foo, perhaps it is because you've missed
dnl 	   the build steps described in the file README, found in the top 
dnl	   source directory of this project.
dnl        
dnl

## You probably don't need to edit this file. Rather, the file build.mk,
## included by this Makefile, is intended for you to write your custom
## build script.

include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

AUXDIR =../../tools#

DOCM4_RELEVANT_RULES

include build.mk

UPDATE_MAKEFILE

EXPORT_FILES += Makefile build.mk README

DOCM4_EXPORT(tyos,0.1.0)

DOCM4_MAKE_BINTOOLS

