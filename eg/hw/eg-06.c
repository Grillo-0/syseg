/* Boot, say hello and halt. 
   Using extended assembly.
*/

#include <eg-06.h>

const char msg[]  = "Hello world";

 
void __attribute__((naked)) _start()   /* This will be a label in asm code. */
{
  
  write_str(msg);	    /* This will be a function call in asm code. */

  halt();                   /* This will be a function call in asm code. */
}



/* Notes

   This code looks like eg-06.c, but functions are implemented with
   GCC's extended inline assembly.

 */
