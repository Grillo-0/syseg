/* Boot, say hello and halt. 
   Using macro-like functions - This code does not work
*/

/*  The string (this is a problem; see bellow). */

const char msg[]  = "Hello World";

void __attribute__((naked)) _start()   /* This will be a label in asm code. */
{

__asm__("\
        mov   $0x0e,%ah               \n \
        mov   $0x0, %bx               \n \
loop:                                 \n \
        mov   msg(%bx), %al           \n \
        cmp   $0x0, %al               \n \
        je    halt                    \n \
        int   $0x10                   \n \
        add   $0x1, %bx               \n \
        jmp   loop                    \n \
                                      \n \
  halt:                               \n \
        hlt                           \n \
        jmp   halt                    \n \
");
  
 
}


/* The boot signature. */

__asm__(".fill 510 - (. - _start), 1, 0");  /* Pad with zeros */
__asm__(".byte 0x55, 0xaa");                /* Boot signature  */


/* Notes

   Asm statements inside __asm__() are ouput as are.

   The attribute 'naked' is used to prevent GCC from generating extra asm
   code that we don't need in our example, and can be safely discarded for
   easy of readability. That extra code is normally important for implementing
   consistent memory utilization across functions which exchange data using
   the memory stack (a memory region which programs use to pop and push
   data when they need). This is not the case of our simple program and we
   therefore may safely get along without it for now. We should get back 
   to this topic opportunely.

   We have a serious problem here: gcc will allocate the initialized string
   at the begining of the program, before the executable code. When BIOS 
   eventually transfers the CPU control to our program, its execution
   starts from the very first byte, which is now the string.

 */
