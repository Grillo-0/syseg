;; Stack method                                                                                          
;; Version 3: alternative solution with x86 base pointer                                       
                                                           

global _start

section .text

_start:

        push 2                  ; Push second operand onto the stack                                             
        push 5                  ; Push first operand onto the stack                                              
        call my_func            ; Pushes return address and jump to my_func                                      

        mov ebx, eax            ; Exit with computed value                                                       
        mov eax, 1
        int 0x80

my_func:
        mov ebp, esp            ; Save original esp                                                              
        

        mov eax, [esp + 4]      ; Read first operand                                                             
        mov ebx, [esp + 8]      ; Read first operand                                                             
        sub eax, ebx            ; Sub in eax, the return register 
                                               
        mov esp, ebp            ; restore original esp              

        ret






