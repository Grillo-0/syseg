#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>

#define MAX 512

int main()
{
  int pid, status;
  char buff[MAX];

  while(1)
    {
      printf ("Prompt: ");
      scanf ("%s", buff);

      pid = fork ();

      if (pid>0)
        {
          wait (&status);
        }
      else
        {
          execvp (buff, (char*[]) {buff, NULL});
          fprintf (stderr, "Command not found.\n");
        }
    }
  return 0;
}
