


 SYSeg Manual
==============================

TL;DR
>          During my undergraduate research program, working with scientific 
	      instrumentation at the Institute of Physics, there was a intimidating
		  huge, high-tech looking piece of equipment for microscopy at the 
		  crystallography lab, that mostly resembled some artifact from a 
		  star-trek episode. A stainless-steel tag attached o it said: 
		  "If after all attempts this doesn't work, read the manual"


 No one likes to read manuals.
 
 I'm sure this one will save you time, though.

PREPARE SYSeg
------------------------------
 
If you've came across a file called README, please, as the file name suggests, __do__ read it; that will save yout time.

Some examples need auxiliary artifacts which must be built beforehand.

If you have obtained the project source from the __version control repository__,

i.e. you've cloned the project from its official Git repository, execute the script 

 ```
 $ ./bootsrap.sh
 ```

to boostrap the build configuration script `configure`. To that end, you'll 
need to have GNU Build System (Autotools) installed. In Debian/Ubuntu based 
platforms,  you may install the required software with

```
$ sudo apt install automake autoconf
```

On the other hand, if you have obtained the software form a __distribution 
repository__, usually as a tarball, you should have the  script `configure`
already built, and therefore you may safely skip the previous step.

Either way, locate the file in the root of source directory tree and execute it

```
 $ ./configure
```

This script shall perform a series of tests to collect data about the build 
platform. If it complains about missing pieces of software, install them 
as needed and rerun `configure`.

Finally, build the software with

```
 $ make
 ```

**Note** that, as mentioned, this procedure is intended to build the auxiliary tools.
The examples and programming exercises themselves will not be built as result --- 
as the process of building those pieces of software is part of the knowledge they 
are meant to illustrate.

To build each example or programming exercise, one need to acess the directories containing the respective source code and follow the instructions indicated in the companion README file.

For more detailed instructions on building SYSeg, please, refer to file `INSTALL` in the root of the source tree.

 


 CONFIGURE SYSeg
 ------------------------------

 Options you may want to configure.

 Configure diff tool
 ------------------------------

   SYSeg uses a graphical diff tool to help visual comparison among code artifacts.
   You may choose which tool you prefer by editing

      DIFF_TOOL = meld

  in syseg/tools/makefile.utils

  
