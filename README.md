# SYSeg - System Software by Example

SYSeg is a collection of source code examples and programming exercises intended as a companion
resource to illustrate general concepts and techniques of system software design and development.


## Quick start


- Some examples need auxiliary artifacts which must be built beforehand: 
see section _Quick Install_ below for directions.

- Each subdirectory contains a `README` file with detailed information about its
contents and specific instructions on compiling, executing and inspecting the examples.

    **Tip**: as the name suggests, please __do read it__ --- that will save you time.

    Files named `README.m4` are not what you're looking for; they are documentation source files.
    If you come across such a file but find no corresponding `README`, that means  you have not built 
    SYSeg properly:  go through the build instructions and perform the indicated steps.

- For copyright and licensing information, please refer to the file 'COPYING' at the root of the
  project's source tree.

## Build SYSeg

 If you have obtained the project source from the __version control repository__,

 i.e. you've cloned the project from its official Git repository, execute the script 

 ```
 $ ./bootsrap.sh
 ```

to boostrap the build configuration script `configure`. To that end, you'll 
need to have GNU Build System (Autotools) installed. In Debian/Ubuntu based 
platforms,  you may install the required software with

```
$ sudo apt install automake autoconf
```

On the other hand, if you have obtained the software from a __distribution 
repository__, usually as a tarball, you should have the  script `configure`
already built, and therefore you may safely skip the previous step.

Either way, locate the file in the root of source directory tree and execute it

```
 $ ./configure
```

This script shall perform a series of tests to collect data about the build 
platform. If it complains about missing pieces of software, install them 
as needed and rerun `configure`.

Finally, build the software with

```
 $ make
 ```

**Note** that, as mentioned, this procedure is intended to build the auxiliary tools.
The examples and programming exercises themselves will not be built as a result --- 
since the process of building those pieces of software is part of the knowledge they 
are meant to illustrate.

To build each example or programming exercise, one needs to proceed to the directory containing the respective source
code and follow the instructions indicated in the companion README file.

For detailed build configuration options, please, refer to file `INSTALL` in the root of the source tree.

## Overview

SYSeg contents. 

- Directory `eg` contains source code examples.
- Directory `try` contains programming exercises.
- Directory `extra` contains non-standard features, advanced topics, and hacker lore.
- Directory `tools` contains auxiliary tools used by examples and exercises.
- Directory `doc` contains SYSeg documentation.


